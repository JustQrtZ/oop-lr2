﻿#include <iostream>
#include <cstring>
#include <fstream>
#include <iomanip>
#include<ios>
using namespace std;

template <typename T>
struct elements {
    T value;
    struct elements* next;
};
template <typename T>
class ListElements {
public:
    elements<T>* first;
    // friend ostream& operator << <T> (ostream& str, const ListElements<T>& ov);

    ListElements() :first(NULL) {}
    int Count() {
        elements<T>* q = first;
        int i = 0;
        while (q->next != NULL) {
            i++;
            q = q->next;
        }
        return i + 1;
    }
    static void randomOutput(int element)
    {

        switch (rand() % 5)
        {
        case 1:
            cout.unsetf(ios::dec);
            cout.setf(ios::oct | ios::showbase);
            cout << element << " Выводить индикатор основания   системы счисления" << endl;
            cout.unsetf(ios::oct | ios::showbase);
            cout.setf(ios::dec);
            break;
        case 2:
            cout.unsetf(ios::dec);
            cout.setf(ios::oct);
            cout << element << " Вывод в 8-ом формате" << endl;
            cout.unsetf(ios::oct);
            cout.setf(ios::dec);
            break;
        case 3:
            cout.unsetf(ios::dec);
            cout.setf(ios::hex | ios::uppercase);
            cout << element << " Вывод в 16-ом формате, верхний регистр" << endl;
            cout.unsetf(ios::hex | ios::uppercase);
            cout.setf(ios::dec);
            break;
        case 4:
            cout.width(40);
            cout.setf(ios::right);
            cout << element << " По правому краю" << endl;
            cout.unsetf(ios::right);
            break;
        default:
            cout.setf(ios::showpos);
            cout << element << " Обычный вывод со знаком +" << endl;
            cout.unsetf(ios::showpos);
            break;
        }
    }
    void addLast(T d) {
        elements<T>* q = first;
        if (first == NULL) {
            elements<T>* tmp = new elements<T>;
            tmp->value = d;
            tmp->next = NULL;
            first = tmp;
            return;
        }
        while (q != NULL) {
            if (q->next == NULL) {
                addAfter(q, d);
                return;
            }
            q = q->next;
        }
    }

    void addAfter(elements<T>* p, T d) {
        elements<T>* tmp = new elements<T>;
        tmp->value = d;
        tmp->next = p->next;
        p->next = tmp;
        p->next = tmp;
    }

    void Sort() {
        for (int d = (Count() / 2) + 1; d > 0; d--) {
            int i = 0;
            while (i + d < Count()) {
                elements<T>* tmp = first;
                int count = 0;
                while (count < i) {
                    tmp = tmp->next;
                    ++count;
                }
                elements<T>* q = tmp;
                while (count < i + d) {
                    tmp = tmp->next;
                    ++count;
                }
                elements<T>* p = tmp;
                if (q->value > p->value) {
                    T qvalue = q->value;
                    q->value = p->value;
                    p->value = qvalue;
                }
                ++i;
            }
        }
    }
};


template<typename T>
class textFile {

public:void static wtriteInFile(ListElements<T> element)
{
    // ListElements<int> element = new ListElements();
    ofstream outfile("text.txt", ofstream::out);
    elements<T>* current = element.first;
    while (current != NULL)
    {
        outfile << (int)current->value << endl;
        //cout << current->value << endl;
        ListElements<T>::randomOutput((int)current->value);
        //str << current->value << endl;
        current = current->next;
    }
}
};

/*
template<typename T>
ostream& operator << (ostream& str, const ListElements<T>& ov)
{
    elements<T>* current = ov.first;
    while (current != NULL)
    {
        ListElements<T>::randomOutput((int)current->value);
        //str << current->value << endl;
        current = current->next;
    }
    return str;
}
*/
int main()
{
    setlocale(LC_ALL, "RUS");
    ListElements <int> spsk1;
    spsk1.addLast(99);
    spsk1.addLast(28);
    spsk1.addLast(54);
    spsk1.addLast(57);
    spsk1.Sort();
    textFile<int>::wtriteInFile(spsk1);
    //textFile<int> a;
    //textFile<int>::wtriteInFile();
    //a.wtriteInFile();
    //a.wtriteInFile(spsk1);
    //textFile::wtriteInFile(spsk1);
    //textFile<int> a;
    //writeInFile
    //cout << spsk1;
}